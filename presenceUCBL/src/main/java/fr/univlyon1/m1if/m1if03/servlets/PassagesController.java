package fr.univlyon1.m1if.m1if03.servlets;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import fr.univlyon1.m1if.m1if03.classes.GestionPassages;
import fr.univlyon1.m1if.m1if03.classes.Passage;
import fr.univlyon1.m1if.m1if03.classes.Salle;
import fr.univlyon1.m1if.m1if03.classes.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@WebServlet(name = "PassageController", urlPatterns = {"/passages", "/passages/*"})
public class PassagesController extends HttpServlet {

    GestionPassages passages;
    Map<String, User> users;
    Map<String, Salle> salles;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.passages = (GestionPassages) config.getServletContext().getAttribute("passages");
        this.users = (Map<String, User>) config.getServletContext().getAttribute("users");
        this.salles = (Map<String, Salle>) config.getServletContext().getAttribute("salles");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path[] = request.getRequestURI().split("/");
        int n = path.length;

        if (path.length != 0) {
            // /passages
            if (path[n-1].equals("passages")) {
                List<Passage> passageList = passages.getAllPassages();
                List<String> passageListURI = new ArrayList<>();
                for (Passage p : passageList) {
                    passageListURI.add(request.getRequestURL() + "/" + p.getId());
                }
                response.setStatus(HttpServletResponse.SC_OK);
                request.setAttribute("passagesAffiches", passageList);
                printJsonResponse(passageListURI, response);
                return;
            }

            // /passages/{passageId}
            if (path[n-2].equals("passages")) {
                int passageID = Integer.parseInt(path[n-1]);
                Passage passage = getPassageByID(passageID);
                if (passage != null) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    printJsonResponse(passage, response);
                } else {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
                return;
            }

            // /passages/byUser/{userId}
            if (path[n-3].equals("passages") && path[n-2].equals("byUser")) {
                String userLogin = path[n-1];

                User us = getUserByLogin(userLogin);
                if(us == null){
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    List<Passage> passageList = passages.getPassagesByUser(new User(userLogin));
                    List<String> passageListURI = new ArrayList<>();
                    for (Passage p : passageList) {
                        passageListURI.add(getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/" + p.getId());
                    }
                    response.setStatus(HttpServletResponse.SC_OK);
                    printJsonResponse(passageListURI, response);
                }
                return;
            }

            // /passages/bySalle/{salleId}
            if (path[n-3].equals("passages") && path[n-2].equals("bySalle")) {
                String salleName = path[n-1];

                Salle s = getSalleByName(salleName);
                if(s == null){
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    List<Passage> passageList = passages.getPassagesBySalle(new Salle(salleName));
                    List<String> passageListURI = new ArrayList<>();
                    for (Passage p : passageList) {
                        passageListURI.add(getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/" + p.getId());
                    }
                    response.setStatus(HttpServletResponse.SC_OK);
                    printJsonResponse(passageListURI, response);
                }
                return;
            }


            // /passages/byUser/{userId}/enCours
            if (path[n-4].equals("passages") && path[n-3].equals("byUser") && path[n-1].equals("enCours")) {
                String userLogin = path[n-2];

                User us = getUserByLogin(userLogin);
                if(us == null){
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    List<Passage> passageList = passages.getPassagesByUserEncours(new User(userLogin));
                    List<String> passageListURI = new ArrayList<>();
                    if(passageList != null){
                        for (Passage p : passageList) {
                            passageListURI.add(getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/" + p.getId());
                        }
                    }
                    response.setStatus(HttpServletResponse.SC_OK);
                    printJsonResponse(passageListURI, response);
                }
                return;
            }


            //   /passages/byUserAndSalle/{userId}/{salleId}
            if (path[n-4].equals("passages") && path[n-3].equals("byUserAndSalle")) {
                String userLogin = path[n-2];
                String salleName = path[n-1];

                User us = getUserByLogin(userLogin);
                Salle s = getSalleByName(salleName);

                if(s == null || us == null){
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    List<Passage> passageList = passages.
                            getPassagesByUserAndSalle(new User(userLogin), new Salle(salleName));
                    List<String> passageListURI = new ArrayList<>();

                    for (Passage p : passageList) {
                        passageListURI.add(getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/" + p.getId());
                    }
                    response.setStatus(HttpServletResponse.SC_OK);
                    printJsonResponse(passageListURI, response);
                }
                return;
            }

            //  /passages/byUserAndDates/{userId}/{dateEntree}/{dateSortie}

            if (path[n-5].equals("passages") && path[n-4].equals("byUserAndDates")) {
                String userLogin = path[n-3];

                User us = getUserByLogin(userLogin);
                if(us == null){
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    try {
                        Date   dateEntree = parseDate(decodeValue(path[n-2]));
                        Date   dateSortie = parseDate(decodeValue(path[n-1]));


                        List<Passage> passageList = passages.
                                getPassagesByUserAndDates(new User(userLogin), dateEntree, dateSortie);
                        List<String> passageListURI = new ArrayList<>();


                        for (Passage p : passageList) {
                            passageListURI.add(getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/" + p.getId());
                        }
                        response.setStatus(HttpServletResponse.SC_OK);
                        printJsonResponse(passageListURI, response);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
                return;
            }

            // /passages/bySalleAndDates/{salleId}/{dateEntree}/{dateSortie}
            if (path[n-5].equals("passages") && path[n-4].equals("bySalleAndDates")) {
                String salleName = path[n-3];
                Salle s = getSalleByName(salleName);
                if(s == null){
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    try {
                        Date   dateEntree = parseDate(decodeValue(path[n-2]));
                        Date   dateSortie = parseDate(decodeValue(path[n-1]));

                        List<Passage> passageList = passages.
                                getPassagesBySalleAndDates(new Salle(salleName), dateEntree, dateSortie);
                        List<String> passageListURI = new ArrayList<>();

                        for (Passage p : passageList) {
                            passageListURI.add(getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/" + p.getId());
                        }
                        response.setStatus(HttpServletResponse.SC_OK);
                        printJsonResponse(passageListURI, response);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                return;
            }
        }

    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/interface.jsp");
        dispatcher.include(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String path[] = request.getRequestURI().split("/");
        int n = path.length;
        List<Salle> salleLists = new ArrayList<>();
        for (String key : salles.keySet()) {
            salleLists.add(salles.get(key));
        }
        if (path.length != 0) {
            // /passages
            if (path[n-1].equals("passages")) {
                // User user = (User) session.getAttribute("user");
                // String nomSalle = request.getParameter("nomSalle");
                JsonObject dataFromPayload = getDataFromPayload(request);

                String login = dataFromPayload.get("user").getAsString();
                String nomSalle = dataFromPayload.get("salle").getAsString();
                Date dateEntree = null;
                Date dateSortie = null;
                try {
                    dateEntree = parseDate(dataFromPayload.get("dateEntree").getAsString());
                    dateSortie = parseDate(dataFromPayload.get("dateSortie").getAsString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                response.addHeader("DATEENTREE",String.valueOf(dateEntree));
                response.addHeader("DATESORTIE",String.valueOf(dateSortie));

                Salle salle = salles.get(nomSalle);
                //User user = (User) session.getAttribute("user");
                User user = new User(login);

                if(salles.containsKey(nomSalle)){
                    if(dateEntree != null){
                        Passage p = new Passage(user, salle, dateEntree );
                        passages.add(p);
                        salle.incPresent();
                        response.setStatus(HttpServletResponse.SC_CREATED);
                        response.addHeader("Location", getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/" + p.getId());
                    }
                    if(dateSortie != null){
                        List<Passage> passTemp = passages.getPassagesByUserAndSalle(user, salle);
                        for (Passage p : passTemp) { // On mémorise une sortie de tous les passages existants et sans sortie
                            if (p.getSortie() == null) {
                                p.setSortie(dateSortie);
                                salle.decPresent();
                            }
                        }
                        //response.setStatus(HttpServletResponse.SC_OK);
                    }
                } else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        }

    }

    public User getUserByLogin(String login) {
        for (Map.Entry<String, User> entry : users.entrySet())
            if (entry.getKey().equals(login)) {
                return entry.getValue();
            }
        return null;
    }

    public Passage getPassageByID(int index) {
        for (Passage passage : passages.getAllPassages())
            if (passage.getId() == index ) {
                return passage;
            }
        return null;
    }

    public Salle getSalleByName(String salleName) {
        for (Map.Entry<String, Salle> entry : salles.entrySet())
            if (entry.getKey().equals(salleName)) {
                return entry.getValue();
            }
        return null;
    }


    public void printJsonResponse(Object objet, HttpServletResponse response) throws IOException {
        Gson gson = new Gson();

        String jsonResponse =  gson.toJson(objet);
        response.setContentType("application/json");
        PrintWriter printWriter = response.getWriter();
        printWriter.write(jsonResponse);
        printWriter.close();
    }

    // renvoi l'objet JSON qui est dans le payload
    public JsonObject getDataFromPayload(HttpServletRequest request) throws IOException {
        // Read from request
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String paramIn = buffer.toString();

        if(paramIn.contains("=")){
            paramIn = paramIn.replaceAll("=", "\":\"");
            paramIn = paramIn.replaceAll("&", "\",\"");
            String result = "{\"" + paramIn + "\"}";;
            return new Gson().fromJson(result, JsonObject.class);
        }

        return new Gson().fromJson(buffer.toString(), JsonObject.class);
    }
    // pour convertir les dates string recu dans le payload en date
    public Date parseDate(String stringDate) throws ParseException {
        SimpleDateFormat formatDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", new Locale("us"));
        return formatDate.parse(stringDate);
    }

    public static String getBaseUrl(String urlString) {
        if(urlString == null) { return null;}
        try {
            URL url = URI.create(urlString).toURL();
            return url.getProtocol() + "://" + url.getAuthority();
        } catch (Exception e) {
            return null;
        }
    }

    public static String decodeValue(String value) {
        try {
            return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

}
