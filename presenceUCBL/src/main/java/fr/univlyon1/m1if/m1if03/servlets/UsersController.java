package fr.univlyon1.m1if.m1if03.servlets;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.*;
import fr.univlyon1.m1if.m1if03.classes.GestionPassages;
import fr.univlyon1.m1if.m1if03.classes.Passage;
import fr.univlyon1.m1if.m1if03.classes.Salle;
import fr.univlyon1.m1if.m1if03.classes.User;
import fr.univlyon1.m1if.m1if03.utils.PresenceUcblJwtHelper;
import org.apache.taglibs.standard.extra.spath.Token;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@WebServlet(name = "UserController", urlPatterns = {"/users","/users/*"})
public class UsersController extends HttpServlet {
    GestionPassages passages;
    Map<String, User> users;
    Map<String, Salle> salles;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.passages = (GestionPassages) config.getServletContext().getAttribute("passages");
        this.users = (Map<String, User>) config.getServletContext().getAttribute("users");
        this.salles = (Map<String, Salle>) config.getServletContext().getAttribute("salles");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path[] = request.getRequestURI().split("/");
        int n = path.length;

        if (path.length != 0) {
            //  /users/login
              if (path[n-2].equals("users")) {
                    if (path[n-1].equals("login")) {
                        JsonObject dataFromPayload = getDataFromPayload(request);
                        String login = dataFromPayload.get("login").getAsString();
                        String nom = dataFromPayload.get("nom").getAsString();
                        // boolean isAdmin = dataFromPayload.get("admin") != null && dataFromPayload.get("admin").equals("false");

                        boolean isAdmin = dataFromPayload.get("admin").getAsBoolean();

                        User user = new User(login);
                        user.setNom(nom);
                        user.setAdmin(isAdmin);

                        if(!users.containsKey(user.getLogin())){
                            users.put(login, user);
                        }

                        String baseUrl = getBaseUrl(String.valueOf(request.getRequestURL()));

                        //Authentification avec TOKEN JWT
                        PresenceUcblJwtHelper presenceUcblJwtHelper = new PresenceUcblJwtHelper();
                        String jwt = presenceUcblJwtHelper.generateToken(baseUrl + "/users/"+ login,isAdmin,request);
                        response.setHeader( "Authorization", "Bearer " + jwt );
                        Cookie tokenCookies = new Cookie("token", jwt);
                        response.addCookie(tokenCookies);
                        tokenCookies.setMaxAge(60*60);


                        response.addHeader("Location", getBaseUrl(String.valueOf(request.getRequestURL())) + "/users/" + login);
                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                    } else if (path[n-1].equals("logout")) {
                        // logout du user
                    } else {
                            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                            return;
                        }
                    }
           // }
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path [] = request.getRequestURI().split("/");
        int n = path.length;

        if (path.length != 0) {
            // /users
            if (path[n-1].equals("users")) {
                List<String> userURI = new ArrayList<>();
                for (String key : users.keySet()) {
                    userURI.add(request.getRequestURL() + "/" + key);
                }
                printJsonResponse(userURI, response);
                response.setStatus(HttpServletResponse.SC_OK);
                return;
            }

            //  /users/{userId}
            if (path[n-2].equals("users")) {
                String userLogin = path[n-1];
                User user = getUserByLogin(userLogin);
                if (user != null) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    printJsonResponse(user, response);
                    //request.setAttribute("user", userLists);
                } else {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
                return;
            }

            //  /users/{userId}/passages
            if (path[n-3].equals("users")) {
                String userLogin = path[n-2];
                if(path[n-1].equals("passages")){
                    User user = getUserByLogin(userLogin);
                    if (user != null) {
                        response.addHeader("Location",getBaseUrl(String.
                                valueOf(request.getRequestURL())) + "/passages/byUser/" + userLogin);
                        response.setStatus(HttpServletResponse.SC_SEE_OTHER);
                    } else {
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    }
                }
                return;
            }

        }

       // processRequest(request, response);
    }


    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String path [] = request.getRequestURI().split("/");
        int n = path.length;
        if (path.length != 0) {
            // /users/{userId}/nom
            if (path[n-3].equals("users")) {
                String userLogin = path[n-2];
                User user = getUserByLogin(userLogin);
                if(path[n-1].equals("nom")){
                    if(user != null){
                        JsonObject dataFromPayload = getDataFromPayload(request);
                        user.setNom(dataFromPayload.get("nom").getAsString());
                        users.put(user.getLogin(), user);
                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                    } else {
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    }
                }
            }
        }

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/interface_admin.jsp");
        dispatcher.include(request, response);
    }

    public User getUserByLogin(String login) {
        for (Map.Entry<String, User> entry : users.entrySet())
            if (entry.getKey().equals(login)) {
                return entry.getValue();
            }
        return null;
    }

    public void printJsonResponse(Object objet, HttpServletResponse response) throws IOException {
        Gson gson = new Gson();

        String jsonResponse =  gson.toJson(objet);
        response.setContentType("application/json");
        PrintWriter printWriter = response.getWriter();
        printWriter.write(jsonResponse);
        printWriter.close();
    }
    public JsonObject getDataFromPayload(HttpServletRequest request) throws IOException {
        // Read from request
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String paramIn = buffer.toString();

        if(paramIn.contains("=")){
            paramIn = paramIn.replaceAll("=", "\":\"");
            paramIn = paramIn.replaceAll("&", "\",\"");
            String result = "{\"" + paramIn + "\"}";;
            return new Gson().fromJson(result, JsonObject.class);
        }
        return new Gson().fromJson(buffer.toString(), JsonObject.class);
    }

    public static String getBaseUrl(String urlString) {
        if(urlString == null) { return null;}
        try {
            URL url = URI.create(urlString).toURL();
            return url.getProtocol() + "://" + url.getAuthority();
        } catch (Exception e) {
            return null;
        }
    }

}
