package fr.univlyon1.m1if.m1if03.servlets;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import fr.univlyon1.m1if.m1if03.classes.GestionPassages;
import fr.univlyon1.m1if.m1if03.classes.Passage;
import fr.univlyon1.m1if.m1if03.classes.Salle;
import fr.univlyon1.m1if.m1if03.classes.User;

import javax.json.Json;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@WebServlet(name = "SalleConroller", urlPatterns = {"/salles", "/salles/*"})
public class SallesConroller extends HttpServlet {

    GestionPassages passages;
    Map<String, User> users;
    Map<String, Salle> salles;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.passages = (GestionPassages) config.getServletContext().getAttribute("passages");
        this.users = (Map<String, User>) config.getServletContext().getAttribute("users");
        this.salles = (Map<String, Salle>) config.getServletContext().getAttribute("salles");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            String []  path= request.getRequestURI().split("/");
            int n = path.length;


            if (path.length != 0) {
                // /salles
                if (path[n-1].equals("salles")) {
                    List<String> sallesURI = new ArrayList<>();
                    for (String key : salles.keySet()) {
                        sallesURI.add(request.getRequestURL() + "/" + key);
                    }
                    response.setStatus(HttpServletResponse.SC_OK);
                    request.setAttribute("salles", sallesURI);
                    printJsonResponse(sallesURI,response);
                }

                // /salles/{salleID}
                if (path[n-2].equals("salles")) {
                    String salleName = path[n-1];
                    Salle salle = getSalleByName(salleName);
                    if (salle != null) {
                        response.setStatus(HttpServletResponse.SC_OK);
                        printJsonResponse(salle,response);
                        //request.setAttribute("salle", salle);
                    } else {
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    }
                }

                // /salles/{salleId}/passages
                if (path[n-3].equals("salles") && path[n-1].equals("passages")) {
                    String salleName = path[n-2];
                    Salle salle = getSalleByName(salleName);

                    if (salle != null) {
                        response.sendRedirect("/passages/" + salleName);
                        response.setStatus(HttpServletResponse.SC_SEE_OTHER);
                        response.addHeader("Location", getBaseUrl(String.valueOf(request.getRequestURL())) + "/passages/bySalle/" + salleName);
                        response.sendRedirect("/passages/bySalle/" + salleName);
                    } else {
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    }
                }

            }

        //processRequest(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path[] = request.getRequestURI().split("/");
        int n = path.length;
        List<Salle> salleLists = new ArrayList<>();
        for (String key : salles.keySet()) {
            salleLists.add(salles.get(key));
        }
        if (path.length != 0) {
            if (path[n-1].equals("salles")) {
                JsonObject dataFromPayload = getDataFromPayload(request);
                String salleName = dataFromPayload.get("nomSalle").getAsString();

                boolean isNotSalleName = (salleName == null || salleName.length() == 0);

                if(isNotSalleName){
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                Salle newSalle = new Salle(salleName);
                salles.put(salleName , newSalle );
                response.setStatus(HttpServletResponse.SC_CREATED);
                response.addHeader("Location", getBaseUrl(String.valueOf(request.getRequestURL())) + "/salles/" + (salles.get(salleName)).getNom());
                request.setAttribute("salles", salleLists);
                printJsonResponse(newSalle,response);


            }
        }

        //doGet(request, response);
    }



    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path [] = request.getRequestURI().split("/");
        int n = path.length;
        if (path.length != 0) {
            // /salles/{salleId}
            if(path[n-2].equals("salles")){
                String salleName = path[n-1];
                boolean isNotSalleName = (salleName == null || salleName.length() == 0);
                if(!isNotSalleName) {
                    JsonObject dataFromPayload = getDataFromPayload(request);
                    Salle salle = getSalleByName(salleName);
                    int capacitefromPayload = Integer.parseInt(dataFromPayload.get("capacite").getAsString());
                    String salleNamefromPayload = dataFromPayload.get("nomSalle").getAsString();

                    if (salle != null && salleNamefromPayload.equals(salle.getNom())) {
                        salle.setCapacite(capacitefromPayload);
                        salles.put(salleName,salle);
                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                        printJsonResponse(salle,response);
                    } else if(salleName.equals(salleNamefromPayload)){
                        //Salle newSalle = new Salle(salleNamefromPayload);
                        Salle newSalle = new Salle(salleName);
                        newSalle.setCapacite(capacitefromPayload);
                        salles.put(salleName, newSalle);
                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                    }

                } else {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad request");
                }

            }
        }

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path [] = request.getRequestURI().split("/");
        int n = path.length;
        if (path.length != 0) {
          //  /salles/{salleId}
            if(path[n-2].equals("salles")){
                String salleName = path[n-1];
                boolean isNotSalleName = (salleName == null || salleName.length() == 0);
                Salle s = getSalleByName(salleName);

                if(!isNotSalleName) {

                    if(s == null){
                        response.sendError(HttpServletResponse.SC_NOT_FOUND);
                    } else {
                        salles.remove(salleName);
                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                    }
                } else {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        }

       // doGet(request, response);

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/interface_admin.jsp");
        dispatcher.include(request, response);
    }

    public Salle getSalleByName(String salleName) {
        for (Map.Entry<String, Salle> entry : salles.entrySet())
            if (entry.getKey().equals(salleName)) {
                return entry.getValue();
            }
        return null;
    }

    public void printJsonResponse(Object objet, HttpServletResponse response) throws IOException {
        Gson gson = new Gson();

        String jsonResponse =  gson.toJson(objet);
        response.setContentType("application/json");
        PrintWriter printWriter = response.getWriter();
        printWriter.write(jsonResponse);
        printWriter.close();
    }

    // renvoi l'objet JSON qui est dans le payload
    public JsonObject getDataFromPayload(HttpServletRequest request) throws IOException {
        // Read from request
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String paramIn = buffer.toString();

        if(paramIn.contains("=")){
            // recupere les données envoyé par un post (qui se trouve dans le body de la request)
            paramIn = paramIn.replaceAll("=", "\":\"");
            paramIn = paramIn.replaceAll("&", "\",\"");
            String result = "{\"" + paramIn + "\"}";;
            return new Gson().fromJson(result, JsonObject.class);
        }

        return new Gson().fromJson(buffer.toString(), JsonObject.class);

    }
    public static String getBaseUrl(String urlString) {
        if(urlString == null) { return null;}
        try {
            URL url = URI.create(urlString).toURL();
            return url.getProtocol() + "://" + url.getAuthority();
        } catch (Exception e) {
            return null;
        }
    }

}
