package fr.univlyon1.m1if.m1if03.filters;

import fr.univlyon1.m1if.m1if03.servlets.WhiteListClass;
import fr.univlyon1.m1if.m1if03.utils.PresenceUcblJwtHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebFilter(filterName = "AuthorizationFilter")
public class AuthorizationFilter extends HttpFilter {
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws ServletException, IOException {
        PresenceUcblJwtHelper presenceUcblJwtHelper = new PresenceUcblJwtHelper();
        //String uri = req.getRequestURI().substring(req.getContextPath().length());

        WhiteListClass whiteListClass = new WhiteListClass();
        List<String> white = whiteListClass.getWhiteList();

        String path[] = req.getRequestURI().split("/");
        int n = path.length;
        String ul = "/" + path[n-2] + "/" + path[n-1];


        if(ul.equals( "/users/login") && white.contains("/users/login")){
            String token = req.getHeader("Authorization");
            if(token == null){
                resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
            chain.doFilter(req, resp);
        } else {
            String token = req.getHeader("Authorization");
             String[] tabToken = token.split(" ");
            boolean isAdmin = presenceUcblJwtHelper.verifyAdmin(tabToken[1]);
            if(isAdmin){
                chain.doFilter(req, resp);
            } else {
                resp.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }

    }
}