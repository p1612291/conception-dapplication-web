## Conception d'application web

```
KEITA Ibrahima p1612291

BRUN ALOIS p1706811
```

###  TP2

le TP2 est disponible sur : https://192.168.75.79/api/v1/

Description :
- Configuration de Tomcat et de Nginx pour le projet
- Connexion et déconnexion
- Saisie de salles
- Saisie de passages dans une salle
- Affichage des informations d'utilisateur en fonction de son login
- Affichage de la liste des salles
- Lister les etudiants se trouvant dans la même salle au même moment



### TP3

le TP3 est disponible sur : https://192.168.75.79/api/v2/

Description :
- Utilisation de Java Beans
- Pattern chaîne de responsabilité
- Pattern MVC
- Gestion du cache

### TP4

le TP4 est disponible sur : https://192.168.75.79/api/v3/

Description:
- Modification des URLs
nous n'avons pas réussi à faire la négotiation de contenu.
  
### TP5 & TP7 : Optimisation d'une Single-Page Application

Adresse de l'application : https://192.168.75.79/
Description :
- Passage à une SPA
- Gestion à l'aide de requêtes AJAX.
- API requetée : https://192.168.75.61/api/v3

#### 1. Analyse de l'état initial de l'application (Sur Tomcat)
* temps de chargement de la page HTML initiale : 56,71 ms
* temps d'affichage de l'app shell : 149 ms
* temps d'affichage du chemin critique de rendu (CRP) : 151 ms


#### 2. Déploiement des fichiers statiques sur Nginx
* temps de chargement de la page HTML initiale : 24,7 ms ; Amélioration :  43,55 % du temps de base
* temps d'affichage de l'app shell : 105 ms ; Amélioration : 70,4 % du temps de base
* temps d'affichage du chemin critique de rendu (CRP) : 118 ms ; Amélioration : 78,14 % du temps de base

#### 3. Optimisation de l'application

Utilisation d'attributs async et/ou defer pour décaler le chargement de scripts non nécessaires au CRP
* temps de chargement de la page HTML initiale : 23,13 ms ; Amélioration : 40,70 % du temps de base
* temps d'affichage de l'app shell : 112 ms ; Amélioration : 75,09 % du temps de base
* temps d'affichage du chemin critique de rendu (CRP) : 121 ms Amélioration : 80,12 % du temps de base
