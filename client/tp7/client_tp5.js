let myToken;
var userLogin;
let indexTemplate;
let monCompteTemplate;
let compteListTemplate;
let entreeTemplate;
let sortieTemplate;
let passagesTemplate;
let passageTemplate;
let decoTemplate;
let passageEncoursTemplate;
let allPassageByUserTemplate;
let tabPassagesEnCours = [];
let tabAllPassageByUser = [];

$(window).bind('load', () => {

indexTemplate = $('#index').prop('innerHTML');
passageEncoursTemplate = $('#condloop').prop('innerHTML');
allPassageByUserTemplate = $('#passagesList').prop('innerHTML');
monCompteTemplate = $('#monCompte').prop('innerHTML');
compteListTemplate = $('#compteList').prop('innerHTML');
entreeTemplate = $('#entree').prop('innerHTML');
sortieTemplate = $('#sortie').prop('innerHTML');
passagesTemplate = $('#passages').prop('innerHTML');
passageTemplate = $('#passages').prop('innerHTML');
decoTemplate = $('#deco').prop('innerHTML');
routage(location.hash);
if(!myToken) {
    location.hash = "index";
}
 });

$(window).bind('hashchange', () => {
    if(!myToken || location.hash === "index") {
        location.hash = "index";
        routage(location.hash);
        $("#passagesEnCours").hide();
        $("#formLogin").show();
    } else {
        $("#passagesEnCours").show();
        $("#formLogin").hide();
        //$("#errMsg").html("");
        routage(location.hash);
    }
    window.setTimeout(masquernotification, 20000);

});

function masquernotification()
{
    var valueErrMsg = $('#errMsg').val();
    if(valueErrMsg !== null){
        $("#errMsg").html("");
    }
}



function routage(route) {
    $("section").each( function() {
        $(this).hide();
    });
    $(route).show();
}


// Login utilisateur
async function loginFonction() {
    const request = {
        method: 'post',
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
            login: $('#userLogin').val(),
            admin: "false",
        })
    };

    await fetch("https://192.168.75.61/api/v3/users/login", request)
        .then(response => {
            if (response.ok) {
                myToken = response.headers.get('Authorization');
                login = $('#userLogin').val();
                userLogin = login;
                $("#formLogin").hide();
                const myError = new Error("Vous êtes connecté")
                displayError(myError, true);

                const request2 = {
                    method: 'get',
                    headers: new Headers({
                        'Accept': 'application/json',
                        'Authorization': myToken
                    })
                }

                var linkPassageEnCours = "https://192.168.75.61/api/v3/passages/byUser/" + login + "/enCours";
                fetch(linkPassageEnCours, request2)
                    .then(response => {
                        if (response.ok) {
                            response.json().then(function (data) {
                                const passages = data
                                    .map(myUri => {
                                        return {
                                            passagesUri: myUri,
                                        };
                                    });

                                if (passages.length === 0) {
                                    tabPassagesEnCours = [];
                                    location.hash = 'index';
                                    mustacheFunction(passageEncoursTemplate, " ", location.hash.replace('#', ''));

                                } else {
                                    _Objet = iteratePassages(passages, tabPassagesEnCours);
                                    if (_Objet) {
                                        location.hash = 'index';
                                        mustacheFunction(passageEncoursTemplate, _Objet, location.hash.replace('#', ''));
                                    }
                                }
                            })

                        } else if (response.status === 401) {
                            const myError = new Error("Connectez-vous !")
                            displayError(myError, false);
                        } else {
                            const myError = new Error("Une erreur s'est produite dans gestion Passages en cours")
                            displayError(myError, false);
                        }
                    });

                  getPassageEnCours();
            } else {
                const myError = new Error("Erreur Login")
                displayError(myError, false);
            }
        });
    // userLogin = login;
}

// logout utilisateur
function logoutFunction() {
    const request = {
        method: 'post',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': myToken
        })
    };
    fetch("https://192.168.75.61/api/v3/users/logout", request)
        .then(response => {
            if (response.ok){
                myToken = undefined;
                userLogin = undefined;
                location.hash = "index";
                mustacheFunction(indexTemplate, " ", location.hash.replace('#', ''));
                const myError = new Error("Vous êtes déconnecté !")
                displayError(myError, true);
            }
        });

    tabPassagesEnCours = [];
    tabAllPassageByUser = [];

}

function getPassageEnCours(){
    const request2 = {
        method: 'get',
        headers: new Headers({
            'Accept': 'application/json',
            'Authorization':  myToken
        })
    }

    var linkPassageEnCours = "https://192.168.75.61/api/v3/passages/byUser/"+login+"/enCours";
    fetch(linkPassageEnCours, request2)
        .then(response => {
            if(response.ok) {
                response.json().then(function (data) {
                    const passages = data
                        .map(myUri => {
                            return {
                                passagesUri: myUri,
                            };
                        });

                    if(passages.length===0){
                        tabPassagesEnCours = [];
                        location.hash = 'index';
                        mustacheFunction(passageEncoursTemplate, " ", location.hash.replace('#', ''));

                    } else {
                        _Objet =  iteratePassages(passages,tabPassagesEnCours );
                        if (_Objet){
                            location.hash = 'index';
                            mustacheFunction(passageEncoursTemplate, _Objet, location.hash.replace('#', ''));
                        }
                    }
                })

            } else if(response.status === 401){
                const myError = new Error("Connectez-vous !")
                displayError(myError,false);
            } else {
                const myError = new Error("Une erreur s'est produite dans gestion Passages en cours")
                displayError(myError,false);
            }
        });

}


function iteratePassages(passages, tableauCible){
    boucleSurPassages(passages,tableauCible);
    objet = {
        passagesEnCours : tableauCible
    };
    return objet ;
}

function boucleSurPassages(passages, tableauCible){
    const request2 = {
        method: 'get',
        headers: new Headers({
            'Accept': 'application/json',
            'Authorization':  myToken
        })
    }
    passages.forEach(function (entry) {
        passage = {}
        userTemp = {};
        salleTemp = {};

        fetch("https://192.168.75.61/api/v3/"+entry.passagesUri, request2)
            .then(response => {
                if(response.ok) {
                    response.json().then(function(data) {
                        passage = {
                            id : data.id,
                            user: data.user,
                            salle: data.salle,
                            dateEntree : data.dateEntree,
                            dateSortie : data.dateSortie,
                        };

                        if ((passage.id !== undefined) &&(passage.user !== undefined) && (passage.salle !== undefined) &&
                            (passage.dateEntree !== undefined) && (passage.dateSortie !== undefined)) {

                            // renvoi les salles ou l'utilisateur est entrée sans en sortir
                            fetch("https://192.168.75.61/api/v3/"+passage.salle, request2)
                                .then(response => {
                                    if (response.ok) {
                                        response.json().then(function (data) {

                                            salleTemp = {
                                                nomSalle: data.nomSalle,
                                                capacite: data.capacite,
                                                presents: data.presents,
                                                saturee: data.saturee
                                            };

                                            if ((salleTemp.nomSalle !== undefined) &&(salleTemp.capacite !== undefined) && (salleTemp.presents !== undefined) &&
                                                (salleTemp.saturee !== undefined) ) {

                                                pass = {
                                                    id: passage.id,
                                                    user: userLogin,
                                                    salle: salleTemp.nomSalle,
                                                    dateEntree: passage.dateEntree,
                                                    dateSortie: passage.dateSortie,
                                                    presents: salleTemp.presents,
                                                    saturee: salleTemp.saturee
                                                };

                                                if ((pass.id !== undefined) &&(pass.user !== undefined) && (pass.salle !== undefined) &&
                                                    (pass.dateEntree !== undefined) && (pass.dateSortie !== undefined) &&
                                                    (pass.saturee !== undefined) && (pass.presents !== undefined)) {

                                                    if(!containsObject(pass, tableauCible)){
                                                        tableauCible.push(pass);
                                                    }
                                                }
                                            }

                                        });
                                    } else {
                                        const myError = new Error("Une erreur dans iteration passages")
                                        displayError(myError,false);
                                    }
                                })

                        }

                    })
                } else {
                    const myError = new Error("erreur dans iteration des salles")
                    displayError(myError,false);
                }

            });
    })
}

// renvoi tous les passages de l'utilisateur
function getAllPassages(){
    const request1 = {
        method: 'get',
        headers: new Headers({
            'Accept': 'application/json',
            'Authorization':  myToken
        })
    }

    var linkPassageEnCours = "https://192.168.75.61/api/v3/passages/byUser/"+userLogin+"";
    fetch(linkPassageEnCours, request1)
        .then(response => {
            if (response.ok){
                response.json().then(function(data) {
                    const allpassagesByUser = data
                        .map(passUri => {
                            return {
                                passagesUri: passUri,
                            };
                        });

                    const _Objet =  iteratePassages(allpassagesByUser, tabAllPassageByUser);

                    if (_Objet){
                        location.hash = 'passages';
                        mustacheFunction(allPassageByUserTemplate, _Objet, location.hash.replace('#', ''));
                    }

                })
            } else if(response.status === 401){
                const myError = new Error("Connectez-vous !")
                displayError(myError,false);
            } else {
                const myError = new Error("Une erreur s'est produite dans Passages by user")
                displayError(myError,false);
            }
        })

}

// Quand on entre dans une salle
function entree() {
    const request = {
        method: 'post',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization':  myToken
        }),
        body: JSON.stringify({
            user: userLogin,
            salle: $('#salleIdEntree').val(),
        })

    };

    let _salle = $('#salleIdEntree').val();

    fetch("https://192.168.75.61/api/v3/passages", request)
        .then(response => {
            if (response.ok) {
                getPassageEnCours()
                const myError = new Error("Vous etes entrer dans la salle "+ _salle )
                displayError(myError,true);
            }
            if(response.status === 401){
                const myError = new Error("Connectez-vous !")
                displayError(myError,false);
            }
            if(response.status === 404){
                const myError = new Error("Salle inexistante !")
                displayError(myError,false);
            }
        });

}

// Quand on sort d'une salle
function sortie() {
    const request = {
        method: 'post',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization':  myToken
        }),
        body: JSON.stringify({
            user: userLogin,
            salle: $('#salleIdSortie').val(),
            dateSortie: "now"
        })

    };
    let _salle = $('#salleIdEntree').val();

    fetch("https://192.168.75.61/api/v3/passages", request)
        .then(response => {
            if (response.ok) {
                getPassageEnCours()
                const myError = new Error("Vous êtes sortie de la salle "+ _salle )
                displayError(myError,true);
            }
            if(response.status === 401){
                const myError = new Error("Connectez-vous !")
                displayError(myError,false);
            }
            if(response.status === 404){
                const myError = new Error("Salle inexistante !")
                displayError(myError,false);
            }
        });

}


// renvoie les info d'un utilisateur
function getInfoUser() {
    const request = {
        method: 'get',
        headers: new Headers({
            'Accept': 'application/json',
            'Authorization':  myToken
        }),
    };
    fetch("https://192.168.75.61/api/v3/users/"+ userLogin, request)
        .then(response => {
            if(response.ok){
                response.json().then(function (data) {
                    //console.log(data.nom)
                    const user = {
                        login: data.login,
                        admin: data.admin,
                        nom: data.nom,
                    };
                    location.hash = 'monCompte';
                    mustacheFunction(compteListTemplate, user, location.hash.replace('#', ''));
                })

            } else if(response.status === 401 || login === undefined){
                const myError = new Error("Connectez-vous !")
                displayError(myError,false);
            }

        });

}

// Modifie le nom de l'utilisateur quand le champ input change
let newName
function changeName(){
    $('#newName').keyup(function() {
        $('#nom').text($(this).val());
        newName = $(this).val();
    });
}

function editName() {
        const request = {
            method: 'put',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization':  myToken
            }),
            body: JSON.stringify({
                nom: newName //$('#nom').val(),
            })
        };
        fetch("https://192.168.75.61/api/v3/users/"+ userLogin+"/nom", request)
            .then(response => {
                if(response.ok){
                    const myError = new Error("Nom modifié avec success")
                    displayError(myError,true);
                }
            });

}

// Display error
function displayError(myError, bool) {
    if(bool){
        $("#errMsg").html(myError.message).css("color", "green");
    } else {
        $("#errMsg").html(myError.message).css("color", "red");
    }
}

// mustache Function
function mustacheFunction(template,data,elemId) {
    let html = Mustache.to_html(template,data);
    $('#'+elemId).html(html);
}

//Check if objet existe in array
function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].dateEntree === obj.dateEntree) {
            return true;
        }
    }
    return false;
}

